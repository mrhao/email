package sango.mail.controller;
import org.springframework.web.bind.annotation.RestController;

import sango.exception.ResourceNotFoundException;
import sango.mail.TenantContext;
import sango.mail.model.Mail;
import sango.mail.repository.MailRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api")
public class MailController {

	@Autowired
    MailRepository mailRepository;
    
    @PostMapping("/server/{serverId}")
    public Mail createServer(@PathVariable(value = "serverId") Integer serverId, @RequestBody Mail mail) {
    	String serverPrefix = "server";
    	String tenantName = serverPrefix.concat(serverId.toString());    	
    	TenantContext.setCurrentTenant(tenantName);
    	
    	//HashMap<?, ?> list = mail.getAttachment();
    	
        return mailRepository.save(mail);
    }
    
    
    @GetMapping("/server/{serverId}")
    public Mail createMail(@PathVariable(value = "serverId") Integer serverId, @RequestParam Map<String, String> requestParams) {
    	String serverPrefix = "server";
    	String tenantName = serverPrefix.concat(serverId.toString());    	
    	TenantContext.setCurrentTenant(tenantName);
    	
    	Mail mail = new Mail();
    	mail.setType(Integer.parseInt(requestParams.get("type")));
    	mail.setSourceId(Integer.parseInt(requestParams.get("sourceId")));
    	mail.setSourceName(requestParams.get("sourceName"));
    	mail.setTargetId(Integer.parseInt(requestParams.get("targetId")));
    	mail.setContent(requestParams.get("content"));
    	mail.setTitle(requestParams.get("title"));
    	mail.setRead(Boolean.parseBoolean(requestParams.get("read")));
    	mail.setAttachment(requestParams.get("attachment"));
    	    	
        return mailRepository.save(mail);
    }

    //@Autowired   
    @GetMapping("/server/{serverId}/id/{id}")
    public Mail getMailById(@PathVariable(value = "serverId") Integer serverId, @PathVariable(value = "id") Integer id) {
    	
    	String serverPrefix = "server";
    	String tenantName = serverPrefix.concat(serverId.toString());    	
    	TenantContext.setCurrentTenant(tenantName);    	
    	//Object currentTenant = TenantContext.getCurrentTenant();
    	//System.out.println("Current: " + currentTenant);
    	
        return mailRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("Mail", "id", id));
    }

}

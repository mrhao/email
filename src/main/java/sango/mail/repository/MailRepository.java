package sango.mail.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sango.mail.model.Mail;

@Repository
public interface MailRepository extends JpaRepository<Mail, Integer> {

}
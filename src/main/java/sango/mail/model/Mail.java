package sango.mail.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import sango.mail.Attachment;

@Entity
@Table(name = "email")
@EntityListeners(AuditingEntityListener.class)
public class Mail implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int type;
	private int sourceId;
	private String sourceName;
	private int targetId;
	private String content;
	private String ttitle;
	private Date sendtime;
	private boolean iread;
	private Attachment attach;

	/*
	 * public Mail(int type, int sourceId, String sourceName, int targetId, String
	 * title, String content, Date sendTime, List<?> attachment) { //super();
	 * this.type = type; this.sourceId = sourceId; this.sourceName = sourceName;
	 * this.targetId = targetId; this.content = content; this.ttitle = title;
	 * this.iread = false; if (sendTime == null) { this.sendtime = new Date(); }
	 * else { this.sendtime = sendTime; }
	 * 
	 * 
	 * 
	 * if (attachment != null && attachment.size() > 0) { this.attach = new
	 * Attachment(); this.attach.setRewards(attachment); }
	 * 
	 * }
	 */
	public String getDate() {
		Calendar now = Calendar.getInstance();
		Calendar then = Calendar.getInstance();
		then.setTime(this.sendtime);
		int diff = now.get(6) - then.get(6);
		if (diff == 0) {
			return "Hôm nay";
		} else if (diff == 1) {
			return "Hôm qua";
		} else if (diff == 2) {
			return "Hôm trước";
		} else {
			if (diff < 0) {
				diff += 365;
			}

			return diff + " Ngày trước";
		}
	}

	public String getTitle() {
		return this.ttitle;
	}

	public Date getSendTime() {
		return this.sendtime;
	}

	public boolean isRead() {
		return this.iread;
	}

	public boolean getRead() {
		return this.iread;
	}

	public Attachment getAttachment() {
		return this.attach;
	}

	public void setTitle(String title) {
		this.ttitle = title;
	}

	public void setSendTime(Date sendTime) {
		if (sendTime == null) {
			this.sendtime = new Date();
		} else {
			this.sendtime = sendTime;
		}
	}

	public void setRead(boolean read) {
		this.iread = read;
	}

	public void setAttachment(String attachment) {	
		//Reward r = new Reward(attachment);
		List<String> attachTmp = Arrays.asList(attachment.split(","));
		if (attachTmp != null  && attachTmp.size() > 0 ){
			this.attach = new Attachment();
			this.attach.setRewards(attachTmp);
			/*this.attach = new Attachment(attachTmp);*/
		} else {
			this.attach = null;
		}
	}
	
	public void setAttachment(Attachment attachment) {
      this.attach = attachment;
   }

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getSourceId() {
		return this.sourceId;
	}

	public void setSourceId(int sourceId) {
		this.sourceId = sourceId;
	}

	public String getSourceName() {
		return this.sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public int getTargetId() {
		return this.targetId;
	}

	public void setTargetId(int targetId) {
		this.targetId = targetId;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}

package sango.mail;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import sango.drop.Reward;

public class Attachment implements Serializable {
   private static final long serialVersionUID = -3600292135396926487L;
   private int version = 1;
   private List rewards;

   public List getRewards() {
      return this.rewards;
   }

   public void setRewards(List rewards) {
      this.rewards = rewards;
   }

   private void readObject(ObjectInputStream in) {
      try {
         in.readInt();
         int e = in.readInt();
         this.rewards = new ArrayList(e);

         for(int i = 0; i < e; ++i) {
            Reward r = Reward.readObject(in);
            this.rewards.add(r);
         }
      } catch (Exception var5) {
         var5.printStackTrace();
      }

   }

   private void writeObject(ObjectOutputStream out) throws IOException {
      out.writeInt(this.version);
      int size = this.rewards.size();
      out.writeInt(size);
      Iterator var4 = this.rewards.iterator();

      while(var4.hasNext()) {
    	 Reward rw = new Reward();
         Reward r = rw.convertReward((String) var4.next());
         r.writeObject(out);
      }

   }
}

package sango.usertype;

import java.io.Serializable;
import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

public abstract class BlobUserType implements UserType {
	public static int[] SQL_TYPE;

	static {
		SQL_TYPE = new int[] { java.sql.Types.BINARY };// Hibernate.BINARY.sqlType()
	}

	public Object assemble(Serializable sobj, Object obj) throws HibernateException {
		return sobj;
	}

	public Serializable disassemble(Object obj) throws HibernateException {
		return (Serializable) obj;
	}

	public boolean equals(Object obj0, Object obj1) throws HibernateException {
		return obj0 == obj1 ? true : (obj0 != null && obj1 != null ? obj0.equals(obj1) : false);
	}

	public Object replace(Object original, Object target, Object owner) throws HibernateException {
		return target;
	}

	public int hashCode(Object obj) throws HibernateException {
		return obj.hashCode();
	}

	public boolean isMutable() {
		return false;
	}

	public int[] sqlTypes() {
		return SQL_TYPE;
	}
}

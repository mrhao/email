package sango.usertype;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;

import sango.mail.Attachment;
import sango.usertype.BlobUserType;
import sango.util.SerialUtil;

public class AttachUserType extends BlobUserType {
	
   public Object deepCopy(Object obj) throws HibernateException {
      return obj;
   }
   
   @Override
   public Object nullSafeGet(ResultSet resultSet, String[] names, SharedSessionContractImplementor arg2, Object owner) throws HibernateException, SQLException {
      byte[] bytes = resultSet.getBytes(names[0]);
      Attachment r = null;
      if(bytes != null) {
         r = (Attachment)SerialUtil.deSerialize(bytes);
      }

      return r;
   }
   
   @Override
   public void nullSafeSet(PreparedStatement statement, Object obj, int index, SharedSessionContractImplementor arg3) throws HibernateException, SQLException {
      byte[] bytes = SerialUtil.serialize((Attachment)obj);
      if(bytes != null) {
         statement.setBytes(index, bytes);
      } else {
         statement.setNull(index, SQL_TYPE[0]);
      }

   }

   public Class<Attachment> returnedClass() {
      return Attachment.class;
   }
}

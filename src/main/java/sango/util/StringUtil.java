package sango.util;

import java.lang.Character.UnicodeBlock;
import java.util.regex.Pattern;

public class StringUtil {
   static UnicodeBlock[] validChars;

   static {
      validChars = new UnicodeBlock[]{UnicodeBlock.CJK_COMPATIBILITY, UnicodeBlock.CJK_COMPATIBILITY_FORMS, UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS, UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT, UnicodeBlock.CJK_RADICALS_SUPPLEMENT, UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION, UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS, UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A, UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B, UnicodeBlock.MATHEMATICAL_OPERATORS, UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS, UnicodeBlock.BASIC_LATIN};
   }

   public static boolean isNumberString(String value) {
      if(value == null) {
         return false;
      } else {
         for(int i = 0; i < value.length(); ++i) {
            char c = value.charAt(i);
            if(c < 48 || c > 57) {
               return false;
            }
         }

         return true;
      }
   }

   public static byte[] hexStringToByteArray(String s) {
	    int len = s.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                             + Character.digit(s.charAt(i+1), 16));
	    }
	    return data;
	}
   
   public static boolean isMailString(String value) {
      Pattern pattern = Pattern.compile("^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$");
      return pattern.matcher(value).matches();
   }

   static boolean isValidChar(char c) {
      UnicodeBlock ub = UnicodeBlock.of(c);
      UnicodeBlock[] var5 = validChars;
      int var4 = validChars.length;

      for(int var3 = 0; var3 < var4; ++var3) {
         UnicodeBlock cub = var5[var3];
         if(cub == ub) {
            return true;
         }
      }

      return false;
   }

   public static int getLength(String s) {
      int rst = 0;

      for(int i = 0; i < s.length(); ++i) {
         char c = s.charAt(i);
         UnicodeBlock ub = UnicodeBlock.of(c);
         if(ub == UnicodeBlock.BASIC_LATIN) {
            ++rst;
         } else {
            rst += 2;
         }
      }

      return rst;
   }

   public static boolean isValidString(String src) {
      for(int i = 0; i < src.length(); ++i) {
         char c = src.charAt(i);
         if(!isValidChar(c)) {
            return false;
         }
      }

      return true;
   }
}

package sango.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerialUtil {
   public static byte[] serialize(Serializable obj) {
      try {
         ByteArrayOutputStream e = new ByteArrayOutputStream();
         ObjectOutputStream oos = new ObjectOutputStream(e);
         oos.writeObject(obj);
         return e.toByteArray();
      } catch (Exception var3) {
         var3.printStackTrace();
         return null;
      }
   }

   public static Object deSerialize(byte[] bytes) {
      try {
         ByteArrayInputStream e = new ByteArrayInputStream(bytes);
         ObjectInputStream ois = new ObjectInputStream(e);
         return ois.readObject();
      } catch (Exception var3) {
         var3.printStackTrace();
         return null;
      }
   }
}

package sango.util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public interface SerialData extends Serializable {
   Object clone();

   void readObject(ObjectInputStream var1) throws IOException, ClassNotFoundException;

   void writeObject(ObjectOutputStream var1) throws IOException;
}

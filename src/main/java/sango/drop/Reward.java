package sango.drop;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import sango.item.ItemService;
import sango.item.ItemTemplate;

public class Reward {
	public int type;
	public int count;
	public ItemTemplate template;

	public Reward(int type, int count, ItemTemplate item) {
		this.type = type;
		this.count = count;
		this.template = item;
	}

	public Reward convertReward(String rewardStr) {
		if (rewardStr != null && !rewardStr.trim().equals("") && !rewardStr.trim().equals("-1")) {
			String[] params = rewardStr.split("\\|");
			if (params.length == 3) {
				int type = Integer.parseInt(params[0]);
				int id = Integer.parseInt(params[1]);
				int count = Integer.parseInt(params[2]);
				if (type != 0) {
					this.type = type;
					this.count = count;
				} else {
					ItemTemplate template = ItemService.getItemTemplate(id);
					if (template == null) {
						//throw new RuntimeException("new reward item can\'t find");
					}

					this.type = type;
					this.count = count;
					this.template = template;
				}
			}
		}
		return this;

	}

	public Reward(List rewardStr) {
		if (rewardStr.size() == 3) {
			int type = Integer.parseInt(rewardStr.get(0).toString());
			int id = Integer.parseInt(rewardStr.get(1).toString());
			int count = Integer.parseInt(rewardStr.get(2).toString());
			if (type != 0) {
				this.type = type;
				this.count = count;
			} else {
				ItemTemplate template = ItemService.getItemTemplate(id);
				if (template == null) {
					// throw new RuntimeException("new reward item can\'t find");
				}

				this.type = type;
				this.count = count;
				this.template = template;
			}
		}

	}

	public Reward() {
		// TODO Auto-generated constructor stub
	}

	public String getStr() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.type).append("|").append(this.template == null ? -1 : this.template.id).append("|")
				.append(this.count);
		return sb.toString();
	}

	public Reward copy() {
		return new Reward(this.type, this.count, this.template);
	}

	public boolean isSame(Reward r) {
		if (this.type == r.type) {
			if (this.type != 0) {
				return true;
			}

			if (this.template.id == r.template.id) {
				return true;
			}
		}

		return false;
	}

	public static List mergeReward(List list) {
		Reward[] array = new Reward[list.size()];

		for (int i = 0; i < array.length; ++i) {
			array[i] = (Reward) list.get(i);
		}

		return mergeReward(array);
	}

	public static List mergeReward(Reward... rewards) {
		ArrayList result = new ArrayList();
		if (rewards != null) {
			Reward[] var5 = rewards;
			int var4 = rewards.length;

			for (int var3 = 0; var3 < var4; ++var3) {
				Reward r = var5[var3];
				boolean flag = false;
				Iterator var8 = result.iterator();

				while (var8.hasNext()) {
					Reward rr = (Reward) var8.next();
					if (r.isSame(rr)) {
						rr.count += r.count;
						flag = true;
						break;
					}
				}

				if (!flag) {
					result.add(r.copy());
				}
			}
		}

		return result;
	}

	public static Reward readObject(ObjectInputStream in) {
		try {
			int e = in.readInt();
			int count = in.readInt();
			int templateId = in.readInt();
			ItemTemplate template = ItemService.getItemTemplate(templateId);
			Reward reward = new Reward(e, count, template);
			return reward;
		} catch (IOException var6) {
			var6.printStackTrace();
			return null;
		}
	}

	public void writeObject(ObjectOutputStream out) throws IOException {
		out.writeInt(this.type);
		out.writeInt(this.count);
		if (this.template == null) {
			out.writeInt(-1);
		} else {
			out.writeInt(this.template.id);
		}

	}

	public String toString() {
		switch (this.type) {
		case 0:
			return this.template.name + "x" + this.count;
		case 1:
		default:
			return "";
		case 2:
			return "Bạc" + "x" + this.count;
		case 3:
			return "Vàng" + "x" + this.count;
		case 4:
			return "Kinh Nghiệm" + "x" + this.count;
		case 5:
			return "Thể lực" + "x" + this.count;
		case 6:
			return "Độ bền" + "x" + this.count;
		case 7:
			return "Hồn ngọc" + "x" + this.count;
		case 8:
			return "Tướng hồn" + "x" + this.count;
		case 9:
			return "Vinh dự" + "x" + this.count;
		case 10:
			return "Danh vọng" + "x" + this.count;
		case 11:
			return "Điểm trận pháp" + "x" + this.count;
		}
	}

	public class RewardResult {
		public int type;
		public int count;
		public List items = new ArrayList();
	}
}

package sango.email.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sango.email.model.Mail;

@Repository
public interface MailRepository extends JpaRepository<Mail, Integer> {

}